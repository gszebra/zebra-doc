# 分布式锁

## 说明

Zebra提供了基于Redis的分布式锁，以便同个微服务下各个实例做分布式协调。

## 样例

请参考 [TODO](TODO)

## 开发

### 引入依赖

引入如下依赖

```markup
<dependency>
    <groupId>com.guosen</groupId>
    <artifactId>zebra-distributed-lock</artifactId>
    <version>${zebra.version}</version>
</dependency>
```

### 配置

#### 配置样例

```text
zebra.distributed.lock.type=redis
zebra.distributed.lock.redis.host=192.168.1.10
zebra.distributed.lock.redis.port=6379
zebra.distributed.lock.redis.db=0
zebra.distributed.lock.redis.password=passwordOfRedis
```

#### 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.distributed.lock.type | String | 分布式锁类型，当前只支持 redis。 |
| zebra.distributed.lock.redis.host | String | 分布式锁使用的 Redis IP 或者域名 |
| zebra.distributed.lock.redis.port | Integer | 分布式锁使用的 Redis 端口，默认值为 6379 |
| zebra.distributed.lock.redis.db | Integer | 分布式锁使用的 Redis 数据库，默认为 0 |
| zebra.distributed.lock.redis.password | String | 分布式锁使用的 Redis 密码，如果没有密码，则不配置 |

### 代码

```java
DistributedLock distributedLock = DistributedLocks.getLock(lockName, 30L);
if (distributedLock == null) {
    LOGGER.error("Failed to get lock of name : {}", lockName);
    return "Failed to get lock " + lockName;
}

if (!distributedLock.tryLock()) {
    LOGGER.error("Failed to try lock");
    return "Failed to try lock " + lockName;
}

// 注意，必须用try/finally 或者 try/catch/finally来释放锁
try {
    // 干活
    doSomeThingWithinLock();
}
finally {
    distributedLock.unlock();
}
```

注意

1. DistributedLocks.getLock 第二个参数为预估的持有锁时间（为了确保程序挂掉后，锁能够自动释放），单位为秒，请确保该时间和业务执行时间基本一致；
2. 获取锁和尝试加锁都必须检查返回结果，加锁成功后在 finally 里面释放锁。

