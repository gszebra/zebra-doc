# MyBatis

## 说明

Zebra 微服务框架提供了一种简便通过 Mybatis 访问数据库的方法。

## 依赖引入

引入如下依赖

```markup
<dependency>
    <groupId>com.guosen</groupId>
    <artifactId>zebra-database</artifactId>
    <version>${zebra.version}</version>
</dependency>
```

## 代码编写

### Mybatis Mapper XML

根据Mybatis规范编写对应的 mapper XML，mapper 文件需要遵循下面的规范：

* 名称格式必须为 **\*Mapper.xml**
* 放到代码目录下 resources 下

```markup
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.guosen.zebra.demo.database.hello.dao.StudentDao">
    <resultMap type="com.guosen.zebra.demo.database.hello.model.Student" id="StudentMap">
        <result column="name" property="name"/>
        <result column="clz" property="clz"/>
        <result column="description" property="description"/>
    </resultMap>

    <select id="getStudent" resultMap="StudentMap">
        select name, clz, description from t_student where name = #{name}
    </select>

    <update id="updateStudent">
        update t_student set clz = #{clz} where name = #{name}
    </update>

</mapper>
```

### Java 代码

* 定义对应的 Mapper 接口

```java
package com.guosen.zebra.demo.database.hello.dao;

import com.guosen.zebra.demo.database.hello.model.Student;
import org.apache.ibatis.annotations.Param;

public interface StudentDao {
    Student getStudent(@Param("name") String name);

    void updateStudent(@Param("name") String name, @Param("clz") String clz);
}
```

* 使用，其他地方使用 @Autowired 自动注入此 Dao 即可。

## 配置

### 配置样例

```text
zebra.database.url[0]=jdbc:jtds:sqlserver://172.24.140.84:1433/ngum
zebra.database.username[0]=sa
zebra.database.pwd[0]=password
zebra.database.basePackage[0]=com.guosen.zebra.demo.database.hello
zebra.database.dataSourceName[0]=gum
logging.level.com.guosen.zebra.hello.mapper=debug
```

### 配置说明

支持最多 10 个数据源的配置， 0 &lt;= k &lt;= 9

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.database.url\[k\] | String | JDBC URL |
| zebra.database.username\[k\] | String | 数据库用户名 |
| zebra.database.pwd\[k\] | String | 数据库密码 |
| zebra.database.basePackage\[k\] | String | Mybatis 扫描的包 |
| zebra.database.dataSourceName\[k\] | String | 数据源名称，确保唯一 |

## 高级功能

### 批量插入

#### 说明

在插入大量数据的情况下，必须使用 Mybatis 的批量插入模式，否则插入速度会很慢。

通常开发者需要编写如下的代码：

```java
    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    public void batchCreate(List<Student> students) {
        try (SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false)) {
            StudentDao studentDao = sqlSession.getMapper(StudentDao.class);
            for (Student student : students) {
                studentDao.create(student);
            }

            sqlSession.commit();
            sqlSession.clearCache();
        }
    }
```

存在如下问题：

* 重复代码，每个批量插入的地方都要实现如上类似的逻辑
* zebra 框架支持多数据源，当业务微服务使用多个数据源时，会在 Spring 中注入多个 SqlSessionFactory 的 bean，代码必须指定 bean 的名称（框架会使用“SqlSesssionFactory + 数据源名称"作为 bean 的名称），这样业务代码和配置、框架底层的实现紧紧耦合到了一起。

综上，Zebra微服务框架提供了一种简便的批量插入方法，开发者只需在对应的 mapper 接口方法上添加 @BatchInsert 注解，便可由框架代为批量插入。

#### 代码实现

**Mapper接口定义**

```java
import com.guosen.model.Student;
import org.mybatis.zebra.annotation.BatchInsert;

import java.util.List;

public interface StudentDao {
    Student get(String name);

    void create(Student student);

    @BatchInsert
    void batchCreate(List<Student> students);
}
```

如上图，在 batchCreate 的方法上面添加 @BatchInsert 注解，同时必须**确保batchCreate这个方法的只有一个且为 List 类型的参数**。

**Mapper.xml**

Mapper 文件中对应的 id 填上对应的 insert 语句，注意该 insert 的参数类型是 List 的元素的类型，而非 List 类型，一般不用配置。

在调用 Mapper batchCreate 接口时，传入多个待插入的对象，zebra 框架会多次调用 insert 语句，最后提交。

```markup
<mapper namespace="com.guosen.dao.StudentDao">
    <resultMap type="com.guosen.model.Student" id="StudentMap">
        <result column="name" property="name"/>
        <result column="clz" property="clz"/>
        <result column="age" property="age"/>
    </resultMap>

    <insert id="batchCreate">
        insert into t_student (name, age, clz) values(#{name}, #{age}, #{clz})
    </insert>

</mapper>
```

