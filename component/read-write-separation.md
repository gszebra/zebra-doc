# 读写分离

## 说明

如果数据存在如下特征

* 大量并发读，写操作少
* 可以接受一段时间内的数据不一致

数据存在如上的特征时，并且数据库读压力比较大时，优先考虑缓存，比如Redis，缓存的成本比从库小。当应用了缓存之后，数据库的读压力还是存在瓶颈的情况下，就可以采用读写分离。

Zebra 微服务框架引入了 [ShardingSphere](https://shardingsphere.apache.org/) 的 Sharding-JDBC 组件来支持读写分离的功能。Sharding-JDBC 相关说明请参考[官方文档](https://shardingsphere.apache.org/document/current/en/overview)

## 样例

请参考 [TODO](TODO)

## 开发

### 引入依赖

引入如下依赖

```markup
<dependency>
    <groupId>com.guosen</groupId>
    <artifactId>zebra-database</artifactId>
    <version>${zebra.version}</version>
</dependency>
```

_注：将 ${zebra.version} 替换为使用的 Zebra 版本_

### 配置

#### 配置样例

```text
zebra.database.url[0]=jdbc:sqlserver://ip:port;database=masterDbName
zebra.database.username[0]=sa
zebra.database.pwd[0]=passwd
zebra.database.dataSourceName[0]=master
zebra.database.url[1]=jdbc:sqlserver://ip:port;database=slave00DbName
zebra.database.username[1]=sa
zebra.database.pwd[1]=passwd
zebra.database.dataSourceName[1]=slave00
zebra.database.url[2]= jdbc:sqlserver://ip:port;database=slave01DbName
zebra.database.username[2]=sa
zebra.database.pwd[2]=passwd
zebra.database.dataSourceName[2]=slave01
zebra.database.shardingcfg.msds01.basePackage=com.guosen.test.zebra.dao
zebra.database.shardingcfg.msds01.masterslave.master-data-source-name=master
zebra.database.shardingcfg.msds01.masterslave.slave-data-source-names=slave00,slave01
zebra.database.shardingcfg.msds01.masterslave.load-balance-algorithm-type=round_robin
zebra.database.shardingcfg.msds01.props.sql.show=true
```

如上图，**zebra.database.shardingcfg** 为读写分离配置的前缀，**msds01** 为读写分离数据源名称，msds01后为具体的读写分离配置项。

#### 配置说明

和 Sharding-JDBC 基本保持一致，但是配置前缀修改为 zebra.database.shardingcfg._${读写分离数据源名称}_

| 配置项 | 说明 |
| :--- | :--- |
| masterslave.master-data-source-name | 读写分离主库的原始数据源名称 |
| masterslave.slave-data-source-names | 读写分离从库的原始数据源名称列表，支持多个，以逗号隔开 |
| masterslave.load-balance-algorithm-type | slave数据源负载均衡算法类型，支持如下配置 round\_robin : 轮询（默认值） random : 随机 |
| basePackage | 读写分离数据源对应MyBatis的basePackage。 此为zebra定制配置项 |
| props.sql.show | 是否开启SQL打印，取如下的值。只建议在开发及上线初期开启。 true : 开启 false : 不开启（默认值） |

### 代码编写

代码开发和 zebra 数据库一致，所有读 SQL 会走 Slave 数据库，所有 写 SQL 会走 Master 数据库。

### 日志

如果将配置项 _props.sql.show_ 设置为 true，则 Sharding-JDBC 会将读写分离 SQL 详情打印到日志中，可判断 SQL 最终路由到哪个数据源上。可通过关键字 “ShardingSphere-SQL” 进行搜索。此开关在开发阶段用于自验，在生产环境需关闭，否则会打印大量日志，影响性能及占用空间。

