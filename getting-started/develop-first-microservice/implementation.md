# 实现

## 生成微服务框架

1. 点击“代码生成器.exe”
2. 输入相关信息，参考如下表格

| 项 | 值 | 说明 |
| :--- | :--- | :--- |
| 是否是生成API接口\(Y/N\) | N | N 表示生成微服务框架 |
| 请输入要生成的项目名称 | zebra-sample-start-svc1 | 输入接口对应的 artifactId |
| 输入groupId | com.guosen.zebra.sample | groupId |

## 引入依赖

在 pom.xml 里面添加如下依赖

```markup
<dependency>
    <groupId>com.guosen</groupId>
    <artifactId>zebra-core</artifactId>
    <version>${zebra.version}</version>
</dependency>
<dependency>
    <groupId>com.guosen.zebra.sample</groupId>
    <artifactId>zebra-sample-start-svc1-api</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

_注：将 ${zebra.version} 替换为使用的 Zebra 版本_

如上，引入接口定义对应的包 zebra-sample-start-svc1-api

## 代码开发

1.新建包

```java
com.guosen.zebra.sample.start.svc1.service.impl
```

2.在此包下添加实现类HelloServiceImpl

```java
package com.guosen.zebra.sample.start.svc1.service.impl;


import com.guosen.zebra.core.grpc.anotation.ZebraService;
import com.guosen.zebra.sample.start.svc1.model.hello.HelloReply;
import com.guosen.zebra.sample.start.svc1.model.hello.HelloRequest;
import com.guosen.zebra.sample.start.svc1.service.HelloService;

@ZebraService
public class HelloServiceImpl implements HelloService {

    @Override
    public HelloReply sayHello(HelloRequest hellorequest) {
        long currentTime = System.currentTimeMillis();
        String message = String.format("Hi, %s From svc1, time : %d", hellorequest.getName(), currentTime);

        HelloReply helloReply = new HelloReply();
        helloReply.setMessage(message);

        return helloReply;
    }
}
```

如上图，该实现类实现了接口 com.guosen.zebra.sample.start.svc1.service.HelloService，并且添加了 **@ZebraService** 注解，以便对外暴露服务。

**注意** ： 一个微服务只能有一个被 **@ZebraService** 注解的类，也就是对外暴露 gRPC 接口只能通过一个 Service 类来，不能通过多个 Service 类暴露。

3.修改App.java的配置

将

```java
@ZebraConf(confName="com.guosen.examples.service.HelloService")
```

修改为

```java
@ZebraConf(confName="com.guosen.zebra.sample.svc1.HelloService")
```

最终代码如下

```java
package com.guosen;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.guosen.zebra.ZebraRun;
import com.guosen.zebra.core.grpc.anotation.ZebraConf;

@SpringBootApplication
@ZebraConf(confName="com.guosen.zebra.sample.svc1.HelloService")
public class App {
    public static void main(String[] args) throws Exception {
        ZebraRun.run(args, App.class,false);
    }
}
```

## 打包

执行如下命令，打包微服务。

```text
mvn clean package
```

