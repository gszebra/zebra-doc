# 部署

## 部署

将微服务包 zebra-sample-start-svc1.zip 上传到 /tmp 目录下。

然后执行如下命令：

```text
unzip /tmp/zebra-sample-start-svc1.zip /opt/zebra/microservice
rm -rf /tmp/zebra-sample-start-svc1.zip
```

## 启动

执行如下命令，启动微服务：

```text
cd /opt/zebra/microservice/zebra-sample-start-svc1/bin
nohup sh start.sh &
```

