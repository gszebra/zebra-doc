# 配置

## 配置

### 配置样例

下面的配置样例在新建配置的时候会自动生成

```text
zebra.grpc.registryAddress=http://etdcIP1:etcdPort,http://etdcIP2:etcdPort,http://etdcIP3:etcdPort
zebra.grpc.port=50006
```

### 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.grpc.registryAddress | String | 注册中心（Etcd）地址 |
| zebra.grpc.port | Integer | gRPC 端口 |

### 新增配置

1. 访问 Zebra 控制台
2. 配置中心 -&gt; 资源类配置，点击新增按钮。

   填入服务名称，值为：

   ```text
    com.guosen.zebra.sample.start.svc1.service.HelloService
   ```

   其他配置项使用默认的即可。

3. 点击“提交”按钮以保存配置。

