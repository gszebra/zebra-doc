# 验证

## 验证

1. 到 Zebra 控制中心 -&gt; 服务方法测试 -&gt; 服务方法查询
2. 输入 com.guosen.zebra.sample.start.svc1.service.HelloService，点击搜索。
3. 可见到如下结果

| 服务名 | 方法名 | 操作 |
| :--- | :--- | :--- |
| com.guosen.zebra.sample.start.svc1.service.HelloService | sayHello | **测试按钮** |

    4. 点击测试按钮，在请求参数一项，输入如下请求

```javascript
{
    "name" : "FooBar"
}
```

   5. 点击提交按钮，可见到类似如下的结果

```javascript
{"message":"Hi, FooBar From svc1, time : 1574931257481"}
```

