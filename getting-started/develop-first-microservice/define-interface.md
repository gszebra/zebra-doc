# 接口定义

## 说明

Zebra 微服务框架使用 gRPC 协议，每个微服务对外暴露的接口都必须用 Protocol Buffers 来定义。

## 生成接口框架

1. 点击“代码生成器.exe”
2. 输入相关信息，参考如下表格

| 输入项 | 输入值 | 说明 |
| :--- | :--- | :--- |
| 是否是生成API接口\(Y/N\) | Y | Y 表示生成接口 |
| 请输入要生成的项目名称 | zebra-sample-start-svc1 | 输入接口对应的 artifactId |
| 输入groupId | com.guosen.zebra.sample | groupId |

1. 检查

输入上一步信息后，在 out 目录下，可找到 zebra-sample-start-svc1-api 目录。（代码生成器会根据项目名称，在 out 目录下生成 ${项目名称}-api 目录，为标准的 maven 工程。）

## 修改接口定义

Zebra 微服务接口使用 Protocol buffers 来定义。

1. 修改

在 out\zebra-sample-start-svc1\src\main\proto\example 目录下可以看到两个Protobuf文件

* hello.proto
* hello\_service.proto

将hello.proto修改为如下内容

```text
syntax = "proto3";
option java_package = "com.guosen.zebra.sample.start.svc1.model";
option java_outer_classname = "Hello";
package com.guosen.zebra.sample.start.svc1.model;

message HelloRequest {
    string name = 1;
}

// The response message containing the greetings
message HelloReply {
    string message = 1;
}
```

将hello\_service.proto修改为如下内容

```text
syntax = "proto3";
option java_package = "com.guosen.zebra.sample.start.svc1.service";
option java_outer_classname = "HelloServiceProto3";
package com.guosen.zebra.sample.start.svc1.service;
import "example/hello.proto";
service HelloService {
    rpc sayHello (com.guosen.zebra.sample.start.svc1.model.HelloRequest) returns (com.guosen.zebra.sample.start.svc1.model.HelloReply) {}
}
```

 2. 打包

在 zebra-sample-start-svc1-api 目录下，执行 install 命令

```text
mvn clean install
```

