# 简述

## 说明

下面我们通过开发一个简单的 Hello World 微服务来熟悉基于 Zebra 微服务框架的开发流程及方法。

微服务功能很简单，接收一个 name 参数，然后返回 ""Hi, ${name} From svc1, time : ${currentTime}" 格式的字符串。

服务架构如下

![&#x7B2C;&#x4E00;&#x4E2A;&#x5FAE;&#x670D;&#x52A1;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIcDTTNFgRbXJNhoym%2F-LvIdvUI9_bNISrB4acp%2Ffirst_service.png?alt=media)

如上图，“微服务1”为我们即将开发的微服务。

启动时，微服务1会先从配置中心下载微服务配置，然后根据配置找到注册中心，向注册中心 Etcd 注册。

最后，我们通过服务中心的方法测试功能来验证微服务1的接口。

## 开发流程

Zebra 微服务开发流程如下：

![&#x5F00;&#x53D1;&#x6D41;&#x7A0B;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIe4cPfkPiuDHCPYZw%2F-LvIfV3Sc42eUsQoR3dc%2Fdev_process.png?alt=media)

