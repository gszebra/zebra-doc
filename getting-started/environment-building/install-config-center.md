# 安装配置中心

## 配置中心安装

1. 上传 zebra-conf.zip 到 /tmp 目录下
2. 执行如下命令，解压

   ```text
   unzip /tmp/zebra-conf.zip -d /opt/zebra/basic
   rm -rf /tmp/zebra-conf.zip
   ```

3. 修改配置

   ```text
   vi /opt/zebra/basic/zebra-conf/config/application.properties
   ```

根据实际数据库情况修改如下配置项。

| 配置项 | 说明 |
| :--- | :--- |
| zebra.grpc.registryAddress | Etcd地址，格式为 http://etcdIp:port，cluster模式有多个，用逗号隔开。 |
| spring.datasource.url | 数据库的 JDBC URL |
| spring.datasource.username | 数据库用户名 |
| spring.datasource.password | 数据库密码 |

## 配置中心启动

执行如下脚本

```text
cd /opt/zebra/basic/zebra-conf/bin
nohup sh start.sh &
```

