# 初始化数据库

## 数据库安装

配置中心的后端存储使用 MySQL，MySQL 安装参考官方网站。

## 数据库初始化

1. 下载初始化脚本 // TODO 把 github 链接放进来
2. 修改 SQL 值

```sql
INSERT INTO CONF_CENTER (
    TYPE,
    SERVER_NAME,
    SERVER_SCOPE,
    SERVER_SCOPE_NAME,
    CONF_TEXT
)
VALUES
    (
        0,
        'zebra.console',
        'global',
        '',
        'zebra.grpc.registryAddress=http://etcdIp:etcdPort
zebra.grpc.port=50003'
);

INSERT INTO CONF_CENTER (
    TYPE,
    SERVER_NAME,
    SERVER_SCOPE,
    SERVER_SCOPE_NAME,
    CONF_TEXT
)
VALUES
    (
        0,
        'zebra.monitor',
        'global',
        '',
        'zebra.grpc.registryAddress=http://etcdIp:etcdPort
zebra.grpc.port=50003
refresh_interval_ms=10000
server.port=8083'
    );
```

* 对于 SERVER\_NAME 为 zebra.console 的 SQL

  | key | 值 |
  | :--- | :--- |
  | zebra.grpc.registryAddress | 替换 Etcd 地址和端口 |

* 对于 SERVER\_NAME 为 zebra.monitor 的 SQL

  | key | 值 |
  | :--- | :--- |
  | zebra.grpc.registryAddress | 替换 Etcd 地址和端口 |
  | zebra.grpc.port | 修改为 50005 |
  | server.port | 修改为 8085 |

* 连接 MySQL， 执行修改后的 SQL。

