# 安装服务中心

## 服务中心安装

1. 上传 zebra-console.zip 到 /tmp 目录下
2. 执行如下命令，解压

```text
unzip /tmp/zebra-console.zip -d /opt/zebra/basic
rm -rf /tmp/zebra-console.zip
```

## 服务中心启动

执行如下脚本

```text
cd /opt/zebra/basic/zebra-console/bin
nohup sh start.sh &
```

