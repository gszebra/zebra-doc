# 安装监控中心

## 安装

1. 上传 zebra-monitor-console.zip 到 /tmp 目录下
2. 执行如下命令，解压

```text
unzip /tmp/zebra-monitor-console.zip -d /opt/zebra/basic
rm -rf /tmp/zebra-monitor-console.zip
```

## 启动

执行如下脚本

```text
cd /opt/zebra/basic/zebra-monitor-console/bin
nohup sh start.sh &
```

