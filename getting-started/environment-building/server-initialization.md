# 初始化服务器

## 目录初始化

初始化 Zebra 基础目录

```text
mkdir -p /opt/zebra
mkdir -p /opt/zebra/basic
mkdir -p /opt/zebra/microservice
```

## 配置初始化

修改配置 /etc/hosts，添加如下配置，替换{本机器IP}。

```text
{本机器IP} zebra-conf.guosen.com
```

