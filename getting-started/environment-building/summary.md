# 简述

## 说明

本节介绍了 Zebra 开发平台的基础组件和基础服务的安装。

## 部署图

为了快速上手，部署使用单机版本。

部署图如下：

![&#x90E8;&#x7F72;&#x56FE;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-Lv9soD3hseEPin_K0Yt%2F-Lv9tZGUqVq59zq2JT64%2F单机部署图.png?alt=media)

## 安装规划

基础组件

| 组件 | 地址 | 目录 |
| :--- | :--- | :--- |
| MySQL | 192.168.1.10 | /opt/zebra/mysql |
| Etcd | 192.168.1.10 | /opt/zebra/etcd |

基础服务

| 服务名称 | 地址 | 目录 |
| :--- | :--- | :--- |
| zebra-conf（配置中心） | 192.168.1.10 | /opt/zebra/basic/zebra-conf |
| zebra-console（服务中心） | 192.168.1.10 | /opt/zebra/basic/zebra-console |
| zebra-monitor-console（监控中心） | 192.168.1.10 | /opt/zebra/basic/zebra-monitor-console |

