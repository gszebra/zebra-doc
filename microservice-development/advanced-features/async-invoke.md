# 异步调用

## 说明

微服务接口支持异步调用，异步调用后，可通过上下文获取 CompletableFuture 做对应的处理\(get 或者 whenComplete 回调\)。

使用方法：将@ZebraReference 的 **async** 属性设置为 true 即可。

## 代码样例

```java
    @ZebraReference(async=true)
    private HelloService helloService;

    public String sayHello(String name) {
        helloService.sayHello(name);
        CompletableFuture<String> resultFuture = (CompletableFuture<String>) RpcContext.getContext().getFuture();

        // do other thing

        return resultFuture.get();
    }
```

