# ACL 控制

## 说明

通常在信任域内，微服务之间的调用不会做鉴权。

但是对于某些关键的微服务，我们需要对其做保护，限定指定的微服务才有权限做调用。

Zebra 微服务框架提供了微服务 ACL 功能来满足上述需求。

## 服务端配置

### 说明

需要在服务端配置哪些微服务可调用其接口。

### 配置样例

假设

客户端的微服务全名为：com.guosen.zebra.sample.ClientService

要求调用的服务端的微服务全面为:com.guosen.zebra.sample.ServerService

那么，服务端微服务的配置为：

```text
zebra.acl.client.clientName[0]=com.guosen.zebra.sample.ClientService
zebra.acl.client.clientlicense[0]=7e070a88-d9f3-4bbb-a65b-1bc9acaa233f
```

### 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.acl.client.clientName\[k\] | String | 允许的客户端微服务全称。 |
| zebra.acl.client.clientlicense\[k\] | String | 授予客户端的认证串，请设置为较复杂的字符串（比如UUID） |

## 客户端配置

需要在客户端配置要调用的服务端微的鉴权服务信息。

### 配置样例

假设

客户端的微服务全名为：com.guosen.zebra.sample.ClientService

要求调用的服务端的微服务全面为:com.guosen.zebra.sample.ServerService

那么，客户端微服务的配置为：

```text
zebra.acl.client.serviceName[0]=com.guosen.zebra.sample.ServerService
zebra.acl.client.servicelicense[0]=7e070a88-d9f3-4bbb-a65b-1bc9acaa233f
```

### 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.acl.client.serviceName\[k\] | String | 要调用的需鉴权的服务端微服务全称。 |
| zebra.acl.client.servicelicense\[k\] | String | 服务端授予本客户端的认证串，和服务端的zebra.acl.client.clientlicense需保持一致。 |

