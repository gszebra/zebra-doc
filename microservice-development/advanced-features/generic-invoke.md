# 范化调用

## 说明

背景：// TODO 为什么要有范化调用，一般微服务哪些场景会使用到。

需注入 com.guosen.zebra.core.grpc.server.GenericService，然后根据要调用的微服务构建 JSON 格式的的参数。

## 代码样例

```java
    @ZebraReference
    private GenericService genericService;

    public String sayHello(String name) {
        JSONObject parameter = new JSONObject();
        parameter.put("name", name);

        JSONObject result = genericService.$invoke(
                "com.guosen.zebra.sample.start.svc1.service.HelloService",
                ZebraConstants.DEFAULT_GROUP,
                ZebraConstants.DEFAULT_VERSION,
                "sayHello",
                parameter);
    }
```

如上图，通过 @ZebraReference 注入 GenericService，然后使用其 $invoke 接口。

1. 请求参数说明如下：

   | 请求参数 | 说明 |
   | :--- | :--- |
   | serviceName（第一个参数） | 要调用的微服务全称 |
   | group（第二个参数） | 要调用的微服务集群，一般使用默认值 ZebraConstants.DEFAULT\_GROUP |
   | version（第三个参数） | 要调用的微服务版本，一般使用默认值 ZebraConstants.DEFAULT\_VERSION |
   | method（第四个参数） | 微服务接口的方法名 |
   | arg（第五个参数） | 要调用的方法的 JSON 格式的请求参数 |

2. 返回参数

   函数的返回值为 JSON 格式的返回值，key 请参考接口的 PB 定义。

