# 消息大小

## 说明

Zebra 微服务底层通信使用 gRPC，支持配置微服务接受的 gPRC 消息的大小，如果消息大小大于此阈值，则请求会失败。

## 配置样例

```text
zebra.grpc.message.size=4096000
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.grpc.message.size | int | gRPC 消息大小，单位为字节（byte）。 如果不配置，默认为 4MB。 |

