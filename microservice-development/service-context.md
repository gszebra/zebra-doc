# 服务上下文

## 说明

Zebra 微服务使用 RpcContext 作为下上文，可跨线程、不同微服务传递参数。

通过下面两个接口可设置或者获取参数。

```java
com.guosen.zebra.core.common.RpcContext.setAttachment(String key, String value);
com.guosen.zebra.core.common.RpcContext.getAttachment(String key);
```

