# 日志

## 说明

Zebra 微服务框架的日志使用 SLF4J + Log4j 来记录

## 代码

在代码中记录日志样例如下。

```java
private static final Logger LOGGER = LoggerFactory.getLogger(YourClassName.class);

public void hi(String value) {
    LOGGER.info("Received value {}", value);

    // do other thing
}
```

## 配置

代码框架工具会为微服务自动生成 log4j 配置文件，文件路径为 : src\main\resources\log4j2.xml

## 路径

在服务器上，微服务的日志目录为：

```text
/var/log/application/${微服务短名称}
```

| 日志路径 | 说明 |
| :--- | :--- |
| /var/log/application/${微服务短名称}/${微服务短名称}-debug.log | 微服务运行日志 |
| /var/log/application/${微服务短名称}/${微服务短名称}-biz.log | 微服务请求、响应日志 |

