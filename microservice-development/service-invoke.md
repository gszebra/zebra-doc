# 服务调用

## 说明

Zebra 微服务框架下，微服务之间通过 gRPC 进行调用。

调用侧的微服务引入要调用的微服务的接口定义 JAR 包，然后使用 @ZebraReference 注入对应的接口，后续的使用和调用本地代码一致。

## 代码样例

```java
    /**
     * 使用 @ZebraReference 注入接口
     */
    @ZebraReference
    private HelloService helloService;

    public String sayHello(String name) {
        HelloRequest helloRequest = new HelloRequest();
        helloRequest.setName(name);

        // 同本地调用一样
        HelloResponse helloResponse = helloService.sayHello(helloRequest);

        return helloResponse.getResult();
    }
```

