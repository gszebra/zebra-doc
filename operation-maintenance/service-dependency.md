# 服务依赖

## 说明

在微服务架构下，随着业务的发展，微服务的个数会越来越多，它们之间的依赖关系也越来越复杂。

将微服务的依赖自动可视化，可让我们快速找出服务依赖。

Zebra提供了微服务依赖图自动发现功能。

## 查看步骤

1. 登录服务中心 [http://172.24.154.238:8080/index.html](http://172.24.154.238:8080/index.html)
2. 左侧菜单栏 服务依赖 -&gt; 服务依赖
3. 将鼠标放到某个微服务图标上，可显示依赖此微服务和此微服务依赖的微服务。

点击微服务图标，可看到该微服务详情，包括：

* 微服务全称
* 微服务实例IP列表

## 图例

![&#x670D;&#x52A1;&#x4F9D;&#x8D56;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIm4gg8Y6wC9XCKNc7%2F-LvInrs95csy1ZslnPZ4%2F01.server_depends.png?alt=media)

