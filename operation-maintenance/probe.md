# 主动探测

## 说明

服务中心提供 RESTful 接口 Zabbix 检查每个服务接口的健康情况（延时/接口成功）。

服务中心根据配置，每 5 分钟对配置了的接口做调用，记录调用信息到内存中。

## 配置探测微服务方法

1. 登录服务中心
2. 左侧导航栏，服务方法查询 -&gt; 服务方法查询
3. 输入要探测的微服务全称，点击搜索
4. 在搜索结果中，选择要探测的方法，点击"测试"
5. 输入“请求参数”和“期望返回参数”，点击“提交”

## 探测接口

### 探测 URL

```text
http://${服务中心IP}:8080/api/service/zabixSurvey
```

### 探测结果样例

```javascript
{
    "code": 0,
    "msg": "查询完成",
    "data": [
        {
            "service": "com.guosen.foo.bar.HelloService",
            "method": "echo",
            "addr": "192.168.1.10",
            "delay": 0,
            "message": "请求参数未配置",
            "desc": null,
            "monitorTime": "2019-12-04 01:52:59",
            "success": false,
            "expect": false
        },
        {
            "service": "com.guosen.foo.bar.HelloService",
            "method": "sayHi",
            "addr": "192.168.1.10",
            "delay": 9,
            "message": "服务正常",
            "desc": null,
            "monitorTime": "2019-12-04 01:53:00",
            "success": true,
            "expect": false
        }
    ]
}
```

### 探测返回结果说明

| 参数名称 | 类型 | 说明 |
| :--- | :--- | :--- |
| code | int | 0 : 查询成功 1 : 查询失败 |
| msg | String | 查询结果信息 |
| data.service | String | 微服务全称 |
| data.method | String | 探测的方法名 |
| data.addr | String | 探测到的实例地址 |
| data.delay | String | 延时 |
| data.message | String | 该方法查询结果信息 |
| data.desc | String | 描述信息 |
| data.monitorTime | String | 探测时间 |
| data.success | Boolean | 探测是否成功 |
| data.expect | Boolean | 探测的返回值是否与设置的预期结果一致 |

