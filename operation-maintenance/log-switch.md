# 日志开关

## 说明

对于 Zebra 微服务的请求，可打开或者关闭返回值日志的记录。默认为打开。

日志文件位置为 /var/log/application/${微服务名称}/${微服务名称}-biz.log

## 配置样例

```text
zebra.enable.retlog=true
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| zebra.enable.retlog | Boolean | 是否记录服务的返回日志，默认值为 true。 true : 记录 false : 不记录 |

