# 限流

## 说明

**限流**其原理是监控应用流量的 QPS 或并发线程数等指标，当达到指定的阈值时对流量进行控制，以避免被瞬时的流量高峰冲垮，从而保障应用的高可用性。

Zebra 使用 QPS 来进行限流，当微服务实例的 QPS 超过设置的阈值时，系统会直接拒绝新的请求。

## 配置样例

```javascript
[
    {
        "resource": "com.guosen.examples.service.HelloService/sayHello",
        "resourceJson": "com.guosen.examples.service.HelloServiceJSON/sayHello",
        "count": 10
    }
]
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| resource | 字符串 | 服务方法名 |
| resourceJson | 字符串 | 服务范化调用方法名，格式为：${服务名称}JSON/方法名 |
| count | 数字 | 限制的 QPS 数量 |

