# 白名单

## 说明

很多时候，我们需要根据调用方来限制请求是否通过，这时候可以白名单控制的功能。白名单根据资源的请求的 IP 限制请求是否通过。配置白名单后，只有请求的 IP 位于白名单内时才可通过。

## 配置样例

```javascript
[
    {
        "resource": "com.guosen.examples.service.HelloService/sayHello",
        "resourceJson": "com.guosen.examples.service.HelloServiceJSON/sayHello",
        "limitIps": "10.33.81.61,10.33.81.62"
    }
]
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| resource | 字符串 | 服务方法名 |
| resourceJson | 字符串 | 服务范化调用方法名，格式为：${服务名称}JSON/方法名 |
| limitIps | 字符串 | 白名单 IP 列表，有多个 IP 时用逗号隔开。 |

