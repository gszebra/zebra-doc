# 简述

## 说明

Zebra 提供了微服务流量控制功能，和 SpringCloud 不同，开发者无须在代码添加注解，只需添加对应的配置即可。

## 配置入口

服务中心 -&gt; 流量控制

![&#x6D41;&#x91CF;&#x63A7;&#x5236;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIvOHIdK1X3iYlDvSK%2F-LvIxF8Idor2Xc_RIa9C%2F05.flow_control.png?alt=media)

