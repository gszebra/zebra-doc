# 熔断

## 说明

除了限流以外，对调用链路中不稳定的资源进行熔断降级也是保障高可用的重要措施之一。由于调用关系的复杂性，如果调用链路中的某个资源不稳定，最终会导致请求发生堆积。

**熔断降级**会在调用链路中某个资源出现不稳定状态时（例如调用超时或异常比例升高），对这个资源的调用进行限制，让请求快速失败，避免影响到其它的资源而导致级联错误。当资源被降级后，在接下来的降级时间窗口之内，对该资源的调用都自动熔断。

## 配置样例

```javascript
[
    {
        "resource": "com.guosen.examples.service.HelloService/sayHello",
        "resourceJson": "com.guosen.examples.service.HelloServiceJSON/sayHello",
        "count": 10,
        "timeWindow": 10
    }
]
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| resource | 字符串 | 服务方法名 |
| resourceJson | 字符串 | 服务范化调用方法名，格式为：${服务名称}JSON/方法名 |
| count | 数字 | 响应时间的阈值，单位毫秒 |
| timeWindow | 数字 | 熔断时间周期，单位秒 |

当 1s 内持续进入 5 个请求，对应时刻的平均响应时间（秒级）均超过阈值（count，以 ms 为单位），那么在接下的时间窗口（DegradeRule 中的 timeWindow，以 s 为单位）之内，对这个方法的调用都会自动地熔断。

