# 黑名单

## 说明

除了白名单，也可以使用黑名单来限制请求是否通过。

相对白名单，黑名单是一种宽松的控制策略。配置黑名单后，只有请求的 IP 位于黑名单内时才会被拒绝通过。

## 配置样例

```javascript
[
    {
        "resource": "com.guosen.examples.service.HelloService/sayHello",
        "resourceJson": "com.guosen.examples.service.HelloServiceJSON/sayHello",
        "limitIps": "10.33.81.61,10.33.81.62"
    }
]
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| resource | 字符串 | 服务方法名 |
| resourceJson | 字符串 | 服务范化调用方法名，格式为：${服务名称}JSON/方法名 |
| limitIps | 字符串 | 黑名单 IP 列表，有多个 IP 时用逗号隔开。 |

