# 调用链

## 说明

Zebra 集成 Zipkin 记录请求的调用链，记录每次请求经过的微服务信息。

## 配置

```text
opentracing.upd.addr=zipkinIp:zipkinPort/api/v2/spans
```

## 配置说明

| 配置项 | 类型 | 说明 |
| :--- | :--- | :--- |
| opentracing.upd.addr | String | Zipkin 接收上报请求的地址 |

