# 监控

## 说明

当前各种主流微服务框架一般都用 Prometheus 做监控指标采集。

Zebra 基于 Etcd 开发了自有的服务注册中心，默认的 Prometheus 不能动态发现微服务实例做指标采集。所以 Zebra 提供一个 zebra-monitor 的微服务对接 Zebra 服务注册中心，由其采集该注册中心下的微服务实例监控指标，最后统一暴露给 Prometheus 进行采集。

架构图如下：

![&#x76D1;&#x63A7;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvEhzSJu7BIF5lcQ1wd%2F-LvEiJhP6-BhTa0WYlpq%2Fzebra_monitor_arch.png?alt=media)

## Promethus 监控信息

### 指标URL

```text
http://zebra-monitor-ip:8084/metrics
```

### 监控指标

| key | 指标名称 | 备注 |
| :--- | :--- | :--- |
| grpc\_server\_started\_total | 接收请求量 |  |
| grpc\_server\_handled\_total | 完成请求量 |  |
| grpc\_server\_msg\_received\_total | 接收消息量 |  |
| grpc\_server\_msg\_sent\_total | 发送消息量 |  |
| grpc\_server\_handling\_ms\_sum | 累计请求处理时间 | 单位：毫秒 |
| grpc\_server\_handling\_ms\_avg | 当前平均延时 | 单位：毫秒 |
| server\_state | 服务状态 | 1 : serving 0 : down |
| active\_thread\_count | 服务活动线程数 |  |

