# 服务查询

## 说明

服务查询包含两个功能

* 服务监控
* 注册中心查询

## 服务监控

服务监控支持每个微服务实例接口的基本指标展示，包括：

* 并发（QPS）
* 时延，单位毫秒
* 状态，是否在服务状态

![&#x670D;&#x52A1;&#x76D1;&#x63A7;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIp05_5_QJCzmVifGN%2F-LvIranqfnOVEPWFvMMU%2F02.service_monitor.png?alt=media)

## 注册中心查询

注册中心查询支持查看微服务实例在 Etcd 中的注册情况，方便运维定位问题。

![&#x6CE8;&#x518C;&#x4E2D;&#x5FC3;&#x67E5;&#x8BE2;](https://blobscdn.gitbook.com/v0/b/gitbook-28427.appspot.com/o/assets%2F-Lv9kABKyc-YZt6JRrpL%2F-LvIp05_5_QJCzmVifGN%2F-LvIrnP__cq4GCRZBMBz%2F03%20service_center_query.png?alt=media)

